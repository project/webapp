Welcome to Webapp

Install Webapp Module:

Place the content of this directory into your modules folder
 
Navigate to administer >> build >> modules. Enable Webapp Module.

Configurate it under /admin/config/content/webapp

if you want to use the "Add-To-Homescreen" functionality you need to download the external addtohomescreen.js
library (3.x-Branch) and place it into sites/all/libraries/addtohomescreen or sites/YOURSITE/libraries/addtohomescreen.
More Infos: http://cubiq.org/add-to-home-screen

Version 3.0.7 has been tested to be working with webapp: https://github.com/cubiq/add-to-homescreen/archive/3.0.7.zip